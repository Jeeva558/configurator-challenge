package com.hps.configurator.constants;

public class ConfiguratorConstants 
{
	public static final String updated_by="CONFIGAPI";
	public static final String Individual_Business_type="individual";
	public static final String Group_Business_type="group";
	public static final String bankAccountTypeId= "bank_account_type_id";
	public static final String bankAccountTypeDesc="bank_account_type_description";
	public static final String bankName="bank_name";
	public static final String bankAccountNumber="bank_account_number";
	public static final String bankAccountType="bank_account_type";
	public static final String businessType="business_type";
	public static final String businessSubType="business_sub_type";
	public static final String Business_Type_URI="configurator/getBusinessType/";
	public static final String StateBusiness_Type_URI="configurator/getEdiStates/";
	
}
