package com.hps.configurator.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.SocketException;
import java.util.*;





@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler {

	//global customize 
	@ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(String errorCode, String errorDesc, Exception ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(errorCode, errorDesc,ex);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	 
	 
	
}