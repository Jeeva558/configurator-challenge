package com.hps.configurator.exception;

public class DbException extends BaseException{
	private String errorCode;
	private String errorDescription;
	private Exception e;
public DbException() {
		super();
	}
	public DbException(String errorCode, String errorDesc, Exception e) {
		super(errorCode, errorDesc, e);
	}
}