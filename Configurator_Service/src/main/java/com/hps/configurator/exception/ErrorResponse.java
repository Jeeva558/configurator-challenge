package com.hps.configurator.exception;

public class ErrorResponse {
	public String errorCode;
	public String errorDesc;
	public Exception e;
	public ErrorResponse(String errorCode2, String errorDesc2, Exception ex) {
		this.errorCode=errorCode2;
		this.errorCode=errorDesc2;
		this.e=ex;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	public Exception getE() {
		return e;
	}
	public void setE(Exception e) {
		this.e = e;
	}
	
}
