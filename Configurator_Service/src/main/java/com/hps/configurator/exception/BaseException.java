package com.hps.configurator.exception;

public class BaseException extends Exception {
public String errorCode;
public String errorDesc;
public Exception e;

public String getErrorCode() {
	return errorCode;
}

public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
}

public String getErrorDesc() {
	return errorDesc;
}

public void setErrorDesc(String errorDesc) {
	this.errorDesc = errorDesc;
}

public Exception getE() {
	return e;
}

public void setE(Exception e) {
	this.e = e;
}

public BaseException(String errorCode, String errorDesc, Exception e) {
	super();
	this.errorCode = errorCode;
	this.errorDesc = errorDesc;
	this.e = e;
}

public BaseException() {
}

}
