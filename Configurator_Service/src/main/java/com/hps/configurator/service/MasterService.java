package com.hps.configurator.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hps.configurator.constants.ConfiguratorConstants;
import com.hps.configurator.dao.MasterDao;
import com.hps.configurator.exception.BaseException;
import com.hps.configurator.exception.DbException;
import com.hps.configurator.model.AllCarriers;
import com.hps.configurator.model.BankInfoObject;
import com.hps.configurator.model.BankView;
import com.hps.configurator.model.BankViewDAO;
import com.hps.configurator.model.BusinessView;
import com.hps.configurator.model.CarrierBusiness;
import com.hps.configurator.model.CarrierInfo;
import com.hps.configurator.model.CarrierRemittance;
import com.hps.configurator.model.CarrierState;
import com.hps.configurator.model.CarrierStateType;
import com.hps.configurator.model.EdiData;
import com.hps.configurator.model.EdiObject;
import com.hps.configurator.model.EdiStates;
import com.hps.configurator.model.LOBRequest;
import com.hps.configurator.model.LegalEntityChild2;
import com.hps.configurator.model.LegalEntityObject1;
import com.hps.configurator.model.LegalEntityObject2;
import com.hps.configurator.model.LegalEntityView;
import com.hps.configurator.model.LegalEntityViewDAO;
import com.hps.configurator.utilities.Utility;



@Service
public class MasterService 
{
 @Autowired
 MasterDao masterDao;
 
 @Autowired
 Utility util;
 
 @Value("${DataServiceDomain}")
 String DataServiceDomain;
 
 public List<String> getCarrierCode()
 {
	 return masterDao.getCarrierCodes();
 }
 
 public List<String> getAllstates()
 {
		/*
		 * Map<String,Boolean> map=new HashMap<String,Boolean>(); List<String>
		 * states=masterDao.getAllStates(); for(String state:states) { map.put(state,
		 * false); } return map;
		 */
	 return masterDao.getAllStates();
 }
 
 public int addcarrier(CarrierInfo carrierinfo)
 {
	 
	 masterDao.addcarrier(carrierinfo.getCarriername(), carrierinfo.getCarriercode());
	 int carrier_id=masterDao.getCarrierId(carrierinfo.getCarriername(),carrierinfo.getCarriercode());
	System.out.println("Updated Of Carrier for carrier code "+carrierinfo.getCarriercode()+"with Carrier_ID"+carrier_id);
	return carrier_id;
 }
 
 public void addRemittanceDetails(CarrierRemittance carrierremit)
 {
	 System.out.println("AddRemittanceDetails Started for "+carrierremit.getCarrier_name());
	int carrier_id=masterDao.getCarrierID(carrierremit.getCarrier_name()+"%");
	 //int carrier_id=masterDao.getCarrierID("liberty%");
	 System.out.println("Carrier id = "+carrier_id);
	Object[] object= {carrier_id,carrierremit.getBill_logo_code(),carrierremit.getBill_question_tel(),carrierremit.getBill_question_website(),carrierremit.getPayable_name(),carrierremit.getRemit_name(),carrierremit.getAddress_line1(),carrierremit.getAddress_line2(),carrierremit.getCity(),carrierremit.getState(),carrierremit.getZip(),""};
	masterDao.addCarrierRemit(object);
	
 }
 
 public Integer getCarrierID(String carriername)
 {
	 return masterDao.getCarrierID(carriername+"%");
 }
 
 public void addCarrierBusiness(CarrierBusiness carrierBusiness)
 {
	int carrier_id=masterDao.getCarrierID(carrierBusiness.getCarriername()+"%");
//		 int carrier_id=masterDao.getCarrierID("liberty%");
	List<String> individual=carrierBusiness.getIndividual();
	List<String> group=carrierBusiness.getGroup();
	
	System.out.println("Individual CarrierBusiness Process Started = "+individual.size());
	for(String type:individual)
	{
		Instant instant=Instant.now();
		 Timestamp time=Timestamp.from(instant);
		
		 int business_type_id=masterDao.selectbusinessTypeId(ConfiguratorConstants.Individual_Business_type, type);
		 System.out.println("Individual_Business_type_id for "+type+" is"+business_type_id);
		Object[] object= {carrier_id,business_type_id,ConfiguratorConstants.updated_by,time};
		masterDao.addCarrierBusiness(object);
	}
	System.out.println("Individual CarrierBusiness Process Ended");
	System.out.println("Group CarrierBusiness Process Started = "+group.size());
	for(String type:group)
	{
		Instant instant=Instant.now();
		 Timestamp time=Timestamp.from(instant);
		 int business_type_id=masterDao.selectbusinessTypeId(ConfiguratorConstants.Group_Business_type, type);
		Object[] object= {carrier_id,business_type_id,ConfiguratorConstants.updated_by,time};
		masterDao.addCarrierBusiness(object);
	}
	System.out.println("Group CarrierBusiness Process Ended");
 }
 
 public String addStates(CarrierState carrierstate)
 {
	 String carrier_name=carrierstate.getCarrier_name();
	 String business_type="";
	 String business_sub_type="";
	 List<CarrierStateType> cst=carrierstate.getCarrierStateType();
	 int carrier_id=masterDao.getCarrierID(carrier_name+"%");
		for(CarrierStateType cst1:cst)
		{
		business_type=cst1.getBusiness_type();
		business_sub_type=cst1.getBusiness_sub_type();
		int business_type_id=masterDao.selectbusinessTypeId(business_type, business_sub_type);
		System.out.println("State Ready to add for "+business_type+"-> "+business_sub_type+"-> "+business_type_id);
		List<String> states=cst1.getStates();
		for(String state:states)
		{
			int owning_carrier_id=masterDao.getOwningCarrierCode(state);
			System.out.println("Owning_Carrier_ID for State "+state+" is "+owning_carrier_id);
			Instant instant=Instant.now();
			 Timestamp time=Timestamp.from(instant);
			Object[] object= {carrier_id,state,business_type_id,owning_carrier_id,ConfiguratorConstants.updated_by,time};
			masterDao.addCarrierStateWide(object);
			
		}
		}
		return "SUCCESS";
 }
 	public List<String> getLOB()
 	{
 		return masterDao.getLineOfBusiness();
 	}
 	
 	public void addLineOfBusiness(LOBRequest lobrequest)
 	{
 		int carrier_id = 0;
 		System.out.println("Line Of Business for "+lobrequest.getCarrier_name()+" is Started");
 		try
 		{
 			carrier_id=masterDao.getCarrierID(lobrequest.getCarrier_name()+"%");
 		}
 		catch(Exception e)
 		{
 			System.out.println(e.getMessage());
 		}
 		System.out.println("Carrier_id = "+carrier_id);
 		List<String> lob=lobrequest.getLob();
 		System.out.println(lob);
 		for(String lob1:lob)
 		{
 			System.out.println(lobrequest.getCarrier_name()+" is mapped to "+lob1);
 			Instant instant=Instant.now();
			 Timestamp time=Timestamp.from(instant);
 			Object[] object= {carrier_id,lob1,ConfiguratorConstants.updated_by,time};
 			masterDao.addLOB(object);
 		}
 		System.out.println("Line Of Business for "+lobrequest.getCarrier_name()+" is Ended");
 	}
 	
 	//Bank Service
 	
 	public List<String> getStateCodeService() {
		return masterDao.getStateCodeDao();
	}
 	
 	public List<String> getBankAccountTypeDescService()
	{
		return masterDao.getBankAccountTypeDescDao();
	}
 	
	public void postBankDetailsService(List<BankInfoObject> bankInfoObjectList) {
		for(BankInfoObject bankInfoObject: bankInfoObjectList)
		{
			 int carrierId=bankInfoObject.getCarrierId();
			 String stateShortName=bankInfoObject.getstateCode();
			 String bankAccountTypeId=bankInfoObject.getBankAccountTypeId();
			 String bankName=bankInfoObject.getBankName();
			 String bankAccountNo=bankInfoObject.getBankAccountNo();
			 String bankNumberCode=bankInfoObject.getbankNumCode();
			 String batchCompanyId=bankInfoObject.getbatchCoId();
			 String batchCompanyName=bankInfoObject.getbatchCoName();
			 String merchantId=bankInfoObject.getmerchantID();
			 String securityKey=bankInfoObject.getSecurityKey();
			 Date securityExpDate=bankInfoObject.getsecurityExpirationDate();
			 int isCarrierAcccountUsed=1;
			 String siteCode=bankInfoObject.getSiteCode(); 
			String updatedBy=ConfiguratorConstants.updated_by;
			 int bankAccountTypeIdDao=masterDao.getBankAccountTypeIdDao(bankAccountTypeId);
			//Null and empty check for the insert variables.
			 if(carrierId!=0 && !stateShortName.isEmpty() && stateShortName!=null && bankAccountTypeIdDao!=0 && !bankName.isEmpty() &&bankName!=null && !bankAccountNo.isEmpty()&&bankAccountNo!=null &&!merchantId.isEmpty() &&merchantId!=null)
				 masterDao.updateBankDetailsService(carrierId,stateShortName,bankAccountTypeIdDao,bankName,bankAccountNo,bankNumberCode,batchCompanyId,batchCompanyName,merchantId,securityKey,securityExpDate,isCarrierAcccountUsed,siteCode,updatedBy);
			 else
			 {
				 System.out.println("Null value ");
			 }
		}
		
	}
	
	//EDI Service
	
	public void postEdiDetailsService(EdiObject ediObject) 
	{
		int carrierId=ediObject.getCarrierId();
		for(EdiData ediData:ediObject.getEdiData())
		{
			String stateShortName=ediData.getStateValue();
			String businessType=ediData.getBusinessTypeValue();			
			String senderId=ediData.getSenderId();
			String receiverId=ediData.getReceiverId();
			String payeeId=ediData.getPayeeId();
			String tpaId=ediData.getTpaId();
			String hiosId=ediData.getHiosId();
			String taxId=ediData.getTaxId();
			String qhpId=ediData.getQhpId();
			String updatedBy=ConfiguratorConstants.updated_by;
			String businessTypeArray[]=businessType.split(",");
			String businessTypeCol=businessTypeArray[0];
			String businessTypeSubTypeCol=businessTypeArray[1];
			int businessTypeId=masterDao.getBusinessTypeIdDao(businessTypeCol,businessTypeSubTypeCol);
			Object[] object= {taxId,hiosId,tpaId,payeeId,qhpId,carrierId,stateShortName,businessTypeId};
			//int owningCarrierId=masterDao.getOwningCarrierIdDao(carrierId,businessTypeId,owningCarrier,stateShortName);
		masterDao.updateCarrierStatewide(object);
		}
	}
	
	public List<String> getBusinessTypeService(int carrierId) {
		List<String> result=new ArrayList<String>();
		List<Map<String,Object>> businessTypes= masterDao.getBusinessTypeDao(carrierId);
		for(Map<String,Object> businessType:businessTypes)
			
		{
			String temp=""+businessType.get(ConfiguratorConstants.businessType)+",";
			temp+=businessType.get(ConfiguratorConstants.businessSubType);
			result.add(temp);
			temp="";
		}
		return result;
	}
	
	public List<String> getEdiStatesService(EdiStates edistates) {
		int carrierId=edistates.getCarrier_id();
		String businessTypeArray[]=edistates.getBusinessTypeValue().split(",");
		String businessTypeCol=businessTypeArray[0];
		String businessTypeSubTypeCol=businessTypeArray[1];
		return masterDao.getEdiStatesDao(carrierId,businessTypeCol,businessTypeSubTypeCol);
	}
	
	public List<String> getOwningCarriersService(int carrierId) {
		return masterDao.getOwningCarrierDao(carrierId);

	}
	
	//LegalEntityService
	
	public void postLegalEntityService(LegalEntityObject1 legalEntityObject) {
		int carrierId=legalEntityObject.getCarrierId();
		int bankID=0;
		String legalEntityName=legalEntityObject.getLegalEntityName();
		String legalEntityCode=legalEntityObject.getLegalEntityCode();
		String stateShortName=legalEntityObject.getState();
		String webUrl=legalEntityObject.getWebsite();
		String updatedBy=ConfiguratorConstants.updated_by;
		String bankInfoArray[]=legalEntityObject.getBankAccountNo().split(",");
		String bankAccountType=bankInfoArray[0];
		String bankName=bankInfoArray[1];
		String bankAccountNumber=bankInfoArray[2];
		bankID=masterDao.getBankId(bankAccountType, bankName, bankAccountNumber);
		int carrierLegalEntityId = masterDao.updateCarrierLegalEntity(carrierId, bankID, legalEntityName, legalEntityCode, stateShortName, webUrl,updatedBy);
		System.out.println(carrierLegalEntityId);
		String addressType=legalEntityObject.getAddressType();
		int addressTypeId=masterDao.getAddressTypeId(addressType);
		String address1=legalEntityObject.getAddressLine1();
		String address2=legalEntityObject.getAddressLine2();
		String city=legalEntityObject.getCity();
		String zipCode=legalEntityObject.getZip();
		String tel=legalEntityObject.getTelePhoneNumber();
		String telExtension=legalEntityObject.getExtension();
		String fax=legalEntityObject.getFaxNumber();
		String email=legalEntityObject.getEmail();
		String workingHours=legalEntityObject.getWorkingHours();
		masterDao.updateCarrierContact(carrierId,carrierLegalEntityId,stateShortName,addressTypeId,address1,address2,city,zipCode,tel,telExtension,fax,email,workingHours,updatedBy);
		}

	public List<String> getBankInfoService(int carrierId) throws DbException,BaseException{
		 List<Map<String,Object>> daoResult=masterDao.getBankInfoDao(carrierId);
		 List<String> bankInfoList=new ArrayList<String>();
		 for(Map<String,Object> bankInfo:daoResult)
		 {
			 String temp="";
			 temp+=bankInfo.get(ConfiguratorConstants.bankAccountType)+",";
			 temp+=bankInfo.get(ConfiguratorConstants.bankName)+",";
			 temp+=bankInfo.get(ConfiguratorConstants.bankAccountNumber);
			 bankInfoList.add(temp);
			 temp="";
		 }
		 		 return bankInfoList;
	}

	public List<String> getAddressType() {
		return masterDao.getAddressTypeDao();
	}
	
	//LegalEntity2
	
	public List<String> getAllLegalEntityStates(int carrierId) {
		return masterDao.getAllLegalEntityStates(carrierId);
	}
	
	public List<String> getAllProductType() {
		return masterDao.getAllProductType();
	}
	
	public List<String> getallProducts(int carrierId) {
		return masterDao.getallProducts(carrierId);
	}
	
	public List<String> getLegalEntityNames(int carrierId) {
		return masterDao.getLegalEntityNames(carrierId);
	}
	public void addLegalEntity2(LegalEntityObject2 legalEntity2)
    {
           System.out.println("Legal Entity 2 Process Started for Carrier ID = "+legalEntity2.getCarrier_id());
           int carrier_id=legalEntity2.getCarrier_id();
           List<LegalEntityChild2> leChild=legalEntity2.getLegalEntityChild();
           for(LegalEntityChild2 le2:leChild)
           {
                  int productCodeID=masterDao.getProductCodeID(le2.getProductcode());
                  System.out.println("ProductCodeID for Product \" "+le2.getProductcode()+" \" is ="+productCodeID);
                  int lineOfBusinessID=masterDao.getLineOfBusinessID(le2.getProductType());
                  System.out.println("LineOfBusinessID for Product \" "+le2.getProductType()+" \" is ="+lineOfBusinessID);
                  int legalEntityID=masterDao.getLegalEntityID(le2.getLegalEntity());
                  System.out.println("LegalEntityID for Product \" "+le2.getLegalEntity()+" \" is ="+legalEntityID);
                  String businessTypeArray[]=le2.getBusinessTypeValue().split(",");
                  String businessTypeCol=businessTypeArray[0];
                  String businessTypeSubTypeCol=businessTypeArray[1];
                  int businessTypeId=masterDao.getBusinessTypeIdDao(businessTypeCol,businessTypeSubTypeCol);
                  System.out.println("BusinessTypeID for Product \" "+le2.getBusinessTypeValue()+" \" is ="+businessTypeId);
                  Object[] object= {productCodeID,le2.getState(),carrier_id,businessTypeId,lineOfBusinessID,legalEntityID,ConfiguratorConstants.updated_by};
                  masterDao.addLegalEntity2(object);
                  
           }
           System.out.println("Legal Entity 2 Process Started for Carrier ID = "+carrier_id);
    }

	public List<AllCarriers> getAllCarrierCode() {
		return masterDao.getAllCarrierCodeDao();
	}

	public List<BusinessView> getStateDetails(int carrierid)
	{
		List<BusinessView> businessview=new ArrayList<BusinessView>();		
		RestTemplate restTemplate = new RestTemplate();
		String businessurl=DataServiceDomain+ConfiguratorConstants.Business_Type_URI+carrierid;
		System.out.println("businessurl to call BusinessController = "+businessurl);
		ResponseEntity<ArrayList> businesstype= restTemplate.getForEntity(businessurl, ArrayList.class);
		List<String> businesslist=businesstype.getBody();
		String statebusinessurl=DataServiceDomain+ConfiguratorConstants.StateBusiness_Type_URI;
		for(String bus:businesslist)
		{
			BusinessView businesschild=new BusinessView();
			String value="";
			businesschild.setBusinessType(bus);
			System.out.println(bus);
			EdiStates edis=new EdiStates();
			edis.setCarrier_id(carrierid);
			edis.setBusinessTypeValue(bus);
			ResponseEntity<ArrayList> states=restTemplate.postForEntity(statebusinessurl, edis, ArrayList.class);
			List<String> statelist=states.getBody();
			for(String state:statelist)
			{
				if(state!=null&&!state.isEmpty())
				{
					value+=state+",";
				}
			}
			if(value!=null&& !value.isEmpty())
			{
			businesschild.setStates(value.substring(0,value.length()-1));
			}
			else
			{
				businesschild.setStates("N/A");
			}
			businessview.add(businesschild);
		}
		
		return businessview;
	}
	
	public List<BankView> getBankDetails(int carrierID)
	{
		List<BankViewDAO> bankdao=masterDao.getBankDetails(carrierID);
		List<BankView> bankview=util.alignBankdetails(bankdao);
		return bankview;
	}
	
	public List<LegalEntityView> getlegalEntityDetails(int carrierId)
	{
		List<LegalEntityViewDAO> ledao=masterDao.getLegalEntityDetails(carrierId);
		List<LegalEntityView> leView = util.alignLegalEntityDetail(ledao);
		return leView;
	}

	public String getIndividualLOB(int carrierId) {
		List<String> ledao=masterDao.getIndividualLOB(carrierId);
		String result="";
		for(String indiv:ledao)
			result+=indiv+",";
		result=result.substring(0, result.length()-1);
		return result;
	}

	public String getCarrierId(String carrierCode, String carrierName) {
		// TODO Auto-generated method stub
		return ""+masterDao.getCarrierId(carrierName,carrierCode);
	}


}
