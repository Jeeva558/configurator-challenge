package com.hps.configurator.daoImpl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.hps.configurator.dao.MasterDao;
import com.hps.configurator.exception.DbException;
import com.hps.configurator.model.AllCarriers;
import com.hps.configurator.model.BankViewDAO;
import com.hps.configurator.model.LegalEntityViewDAO;

@Repository
public class MasterDaoImpl implements MasterDao 
{
	@Autowired
	private JdbcTemplate jdbctemplate;
	
	@Value("${getCarrierCode}")
	String getCarriercodeQuery;
	
	@Value("${insertNewCarrier}")
	String insertNewCarrierQuery;
	
	@Value("${getcarrierID}")
	String getcarrierIDQquery;
	
	@Value("${insertCarrierRemit}")
	String insertBankRemitQuery;
	
	@Value("${getAllStates}")
	String getAllStatesQuery;
	
	@Value("${insertCarrierBusiness}")
	String insertCarrierBusinessQuery;
	
	@Value("${selectbusinessTypeId}")
	String selectbusinessTypeIdQuery;
	
	@Value("${getOwningCarrierCode}")
	String getOwningCarrierCodeQuery;
	
	@Value("${insertStateWide}")
	String insertStateWideQuery;
	
	@Value("${selectlineofbusiness}")
	String selectlineofbusinessQuery;
	
	@Value("${insertLOB}")
	String insertLOBQuery;
	
	@Value("${query_for_state_name}")
	String queryForStateName;
	
	@Value("${query_for_bank_account_type_id}")
	String queryForBankAccountTypeId;
	
	@Value("${query_for_bank_account_type_description}")
	String queryForBankAccountTypeDesc;
	
	@Value("${query_for_update_bank_details}")
	String queryForUpdateBankDetails;
	
	@Value("${query_for_bank_id}")
	String queryForBankId;
	
	@Value("${query_for_businessType}")
	String queryForBusinessType;
	
	@Value("${query_for_edi_states}")
	String queryForEdiStates;
	
	@Value("${query_for_owning_carrier}")
	String queryForOwningCarrier;
	
	@Value("${query_for_businessTypeId}")
	String queryForBusinessTypeId;
	
	@Value("${query_for_owning_carrierId}")
	String queryForOwningCarrierId;
	
	@Value("${query_for_update_carrier_statewide}")
	String queryForUpdateCarrierStatewide;
	
	@Value("${query_for_address_type}")
	String queryForAddressType;
	
	@Value("${query_for_bank_info}")
	String queryForBankInfo;
	
	@Value("${query_for_carrierId}")
	String queryForCarrierId;
	
	@Value("${query_for_update_carrier_legalEntity}")
	String queryForUpdateCarrierLegalEntity;
	
	@Value("${query_for_update_carrier_contact}")
	String queryForUpdateCarrierContact;
	
	@Value("${query_for_address_typeId}")
	String queryForAddressTypeId;
	
	@Value("${getAllProductType}")
	String getAllProductTypeQuery;
	
	@Value("${getAllLEStates}")
	String getAllLEStatesQuery;
	
	@Value("${getallProducts}")
	String getallProductsQuery;
	
	@Value("${getLENames}")
	String getLENamesQuery;
	
	@Value("${addLegalEntity2}")
	 String addLegalEntity2Query;
	
	@Value("${getLegalEntityID}")
	 String getLegalEntityIDQuery;
	
	@Value("${getLineOfBusinessID}")
	 String getLineOfBusinessIDQuery;
	
	@Value("${query_for_individual_lob}")
	String getIndividualLOBQuery;
	
	@Value("${getProductCodeID}")
	 String getProductCodeIDQuery;
	
	@Value("${getAllCarriers}")
	String getAllCarriers;
	
	@Value("${getBankView}")
	String getBankViewQuery;
	
	@Value("${getLegalEntityView}")
	String getLegalEntityViewQuery;
	
	@Override
	public List<String> getCarrierCodes() {
		List<String> carriercode=jdbctemplate.queryForList(getCarriercodeQuery,String.class);
		return carriercode;
	}

	@Override
	public void addcarrier(String CarrierName, String CarrierCode) {
		System.out.println("New Carrier Updation Process Started"+CarrierCode+"for Carrier name "+CarrierName);
		jdbctemplate.update(insertNewCarrierQuery, new Object[] {CarrierName,CarrierName,CarrierCode});
		
	}

	@Override
	public int getCarrierID(String carriername) {
		int carrier_id=jdbctemplate.queryForObject(getcarrierIDQquery, new Object[] {carriername},Integer.class);
		return carrier_id;
	}

	@Override
	public void addCarrierRemit(Object[] object) {
		jdbctemplate.update(insertBankRemitQuery,object);
		
	}

	@Override
	public List<String> getAllStates() {
		List<String> allstates=jdbctemplate.queryForList(getAllStatesQuery,String.class);
		return allstates;
	}

	@Override
	public void addCarrierBusiness(Object[] object) {
		jdbctemplate.update(insertCarrierBusinessQuery,object);
		
	}

	@Override
	public int selectbusinessTypeId(String business_type, String business_sub_type) {
		int businessTypeId=jdbctemplate.queryForObject(selectbusinessTypeIdQuery, new Object[] {business_type,business_sub_type},Integer.class);
		return businessTypeId;
	}

	@Override
	public int getOwningCarrierCode(String state) {
		int owning_carrier_id=jdbctemplate.queryForObject(getOwningCarrierCodeQuery, new Object[] {state},Integer.class);
		return owning_carrier_id;
	}

	@Override
	public void addCarrierStateWide(Object[] object) {
		jdbctemplate.update(insertStateWideQuery,object);
		
	}

	@Override
	public List<String> getLineOfBusiness() {
		List<String> lob=jdbctemplate.queryForList(selectlineofbusinessQuery,String.class);
		return lob;
	}

	@Override
	public void addLOB(Object[] object) {
		jdbctemplate.update(insertLOBQuery,object);
		
	}
	
	@Override
	public int getBankId(String bankAccountType,String bankName,String bankAccountNumber)
	{
		int bankId=jdbctemplate.queryForObject(queryForBankId, new Object[] {bankAccountType,bankName,bankAccountNumber},Integer.class);
		return bankId;
	}
	
	@Override
	public List<String> getStateCodeDao() {
		List<String>listOfStateName=jdbctemplate.queryForList(queryForStateName,String.class);
	return listOfStateName;
	}
	
	@Override
	public int getBankAccountTypeIdDao(String bankAccountTypeId)
	{
		int bankAccountId=jdbctemplate.queryForObject(queryForBankAccountTypeId,new Object[] {bankAccountTypeId},Integer.class);
		return bankAccountId;

	}
	
	@Override
	public List<String> getBankAccountTypeDescDao()
	{
		List<String>listOfBankAccountTypeDesc=jdbctemplate.queryForList(queryForBankAccountTypeDesc,String.class);
		return listOfBankAccountTypeDesc;

	}
	
	@Override
	public void updateBankDetailsService(int carrierId,String stateShortName,int bankAccountTypeId,String bankName,String bankAccountNo,String bankNumberCode,
			String batchCompanyId,String batchCompanyName,String merchantId,String securityKey,Date securityExpDate,int isCarrierAcccountUsed, String siteCode, String updatedBy) {
		jdbctemplate.update(queryForUpdateBankDetails, new Object[] {carrierId,stateShortName,bankAccountTypeId,bankName,bankAccountNo,bankNumberCode,batchCompanyId,batchCompanyName,merchantId,securityKey,securityExpDate,isCarrierAcccountUsed,siteCode,updatedBy});
	}
	
	@Override
	public List<Map<String, Object>> getBusinessTypeDao(int carrierId) {
		List<Map<String,Object>>queryResult=jdbctemplate.queryForList(queryForBusinessType,new Object[] {carrierId});
		return queryResult;
	}
	
	@Override
	public List<String> getEdiStatesDao(int carrierId,String businessTypeCol,String businessTypeSubTypeCol) {
		return jdbctemplate.queryForList(queryForEdiStates,new Object[] {carrierId,businessTypeCol,businessTypeSubTypeCol},String.class);
	}
	
	@Override
	public List<String> getOwningCarrierDao(int carrierId) {
		return jdbctemplate.queryForList(queryForOwningCarrier,new Object[] {carrierId},String.class);
	}
	
	@Override
	public int getBusinessTypeIdDao(String businessTypeCol, String businessTypeSubTypeCol) {
		return jdbctemplate.queryForObject(queryForBusinessTypeId,new Object[] {businessTypeCol,businessTypeSubTypeCol},Integer.class);
	}
	
	@Override
	public int getOwningCarrierIdDao(int carrierId, int businessTypeId, String owningCarrier, String stateShortName) {
		return jdbctemplate.queryForObject(queryForOwningCarrierId,new Object[] {carrierId,businessTypeId,owningCarrier,stateShortName},Integer.class);
	}
	
	@Override
	public void updateCarrierStatewide(Object[] object) {
		jdbctemplate.update(queryForUpdateCarrierStatewide,object);
	}
	
	@Override
	public int getAddressTypeId(String addressType)
	{
		int addressTypeId=jdbctemplate.queryForObject(queryForAddressTypeId, new Object[] {addressType},Integer.class);
		return addressTypeId;
	}
	@Override
	public int getCarrierId(String carrierName,String carrierCode)
	{
		int carrierId=jdbctemplate.queryForObject(queryForCarrierId, new Object[] {carrierName,carrierCode},Integer.class);
		return carrierId;
	}
	
	@Override
	public int updateCarrierLegalEntity(int carrierId,int bankID,String legalEntityName,String legalEntityCode, String stateShortName, String webUrl,String updatedBy)
	{
		KeyHolder keyHolder = new GeneratedKeyHolder();
//		jdbcTemplate.update(queryForUpdateCarrierLegalEntity,new Object[] {carrierId,bankID,legalEntityName,legalEntityCode,stateShortName,webUrl,updatedBy},keyHolder);
//		System.out.println("KeyHolder "+keyHolder);
		jdbctemplate.update(
		connection -> {
            PreparedStatement ps = connection.prepareStatement(queryForUpdateCarrierLegalEntity, new String[]{"carrier_legal_entity_id"});
            ps.setInt(1, carrierId);
            ps.setInt(2, bankID);
            ps.setString(3, legalEntityName);
            ps.setString(4, legalEntityCode);
            ps.setString(5, stateShortName);
            ps.setString(6, webUrl);
            ps.setString(7, updatedBy);
            return ps;
        }, keyHolder);	    
		   System.out.println("KeyHolder "+keyHolder.getKey().intValue());
		return keyHolder.getKey().intValue();
	}
	
	@Override
	public List<String> getAddressTypeDao()
	{
		List<String>listOfAddressType=jdbctemplate.queryForList(queryForAddressType,String.class);
		return listOfAddressType;
	}
	
	@Override
	public List<Map<String, Object>> getBankInfoDao(int carrierId){
		List<Map<String,Object>>listOfBankInfo=null;
		listOfBankInfo=jdbctemplate.queryForList(queryForBankInfo,new Object[] {carrierId});
		
		return listOfBankInfo;
	}
	
	@Override
	public void updateCarrierContact(int carrierId, int carrierLegalEntityId, String stateShortName,
			int addressTypeId, String address1, String address2, String city, String zipCode, String tel,
			String telExtension, String fax, String email, String workingHours,String updatedBy) {
		jdbctemplate.update(queryForUpdateCarrierContact,new Object[] {carrierId,carrierLegalEntityId,stateShortName,addressTypeId,address1,address2,city,zipCode,tel,telExtension,fax,email,workingHours,updatedBy});
	}

	@Override
	public List<String> getAllProductType() {
		List<String> producttype=jdbctemplate.queryForList(getAllProductTypeQuery,String.class);
		return producttype;
	}

	@Override
	public List<String> getAllLegalEntityStates(int carrierId) {
		List<String> states=jdbctemplate.queryForList(getAllLEStatesQuery,new Object[] {carrierId},String.class);
		return states;
	}
	
	@Override
	public List<String> getallProducts(int carrierId) {
		List<String> states=jdbctemplate.queryForList(getallProductsQuery,new Object[] {carrierId},String.class);
		return states;
	}
	
	@Override
	public List<String> getLegalEntityNames(int carrierId) {
		List<String> LEnames=jdbctemplate.queryForList(getLENamesQuery,new Object[] {carrierId},String.class);
		return LEnames;
	}
	@Override
    public int getProductCodeID(String prodTypeCode) {
          
		int productCodeID=jdbctemplate.queryForObject(getProductCodeIDQuery, new Object[] {prodTypeCode},Integer.class);
           return productCodeID;
    }

    @Override
    public int getLineOfBusinessID(String prodType) {
          
		int prodTypeID=jdbctemplate.queryForObject(getLineOfBusinessIDQuery, new Object[] {prodType},Integer.class);
           return prodTypeID;
    }

    @Override
    public int getLegalEntityID(String legalEntity) {
          
		int legalEntityID=jdbctemplate.queryForObject(getLegalEntityIDQuery, new Object[] {legalEntity},Integer.class);
           return legalEntityID;
    }

    @Override
    public void addLegalEntity2(Object[] object) {
          
		jdbctemplate.update(addLegalEntity2Query,object);
           
    }

	@Override
	public List<AllCarriers> getAllCarrierCodeDao() {
	
		List<AllCarriers> carrierCodes=jdbctemplate.query(getAllCarriers,new BeanPropertyRowMapper(AllCarriers.class));
		return carrierCodes;
	}

	@Override
	public List<BankViewDAO> getBankDetails(int carrierId) {
		List<BankViewDAO> bankdao=jdbctemplate.query(getBankViewQuery, new Object[] {carrierId},new BeanPropertyRowMapper(BankViewDAO.class));
		return bankdao;
	}

	@Override
	public List<LegalEntityViewDAO> getLegalEntityDetails(int carrierId) {
		List<LegalEntityViewDAO> leDao=jdbctemplate.query(getLegalEntityViewQuery,new Object[] {carrierId},new BeanPropertyRowMapper(LegalEntityViewDAO.class));
		return leDao;
	}

	@Override
	public List<String> getIndividualLOB(int carrierId) {
		// TODO Auto-generated method stub
		List<String> lobDao=jdbctemplate.queryForList(getIndividualLOBQuery, new Object[] {carrierId},String.class);
		return lobDao;
	}


}
