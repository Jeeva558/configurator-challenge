package com.hps.configurator.model;

public class AllCarriers 
{
	private String carrier_id;
	private String carrier_code;
	private String carrier_full_name;
	
	public String getCarrier_code() {
		return carrier_code;
	}
	public void setCarrier_code(String carrier_code) {
		this.carrier_code = carrier_code;
	}
	public String getCarrier_full_name() {
		return carrier_full_name;
	}
	public void setCarrier_full_name(String carrier_full_name) {
		this.carrier_full_name = carrier_full_name;
	}
	public String getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(String carrier_id) {
		this.carrier_id = carrier_id;
	}
	
	
}
