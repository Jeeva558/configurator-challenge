package com.hps.configurator.model;

import java.util.List;

public class LegalEntityObject2 
{
	private int carrier_id;
	private List<LegalEntityChild2> legalEntityChild;
	
	public int getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(int carrier_id) {
		this.carrier_id = carrier_id;
	}
	public List<LegalEntityChild2> getLegalEntityChild() {
		return legalEntityChild;
	}
	public void setLegalEntityChild(List<LegalEntityChild2> legalEntityChild) {
		this.legalEntityChild = legalEntityChild;
	}
	
	
}
