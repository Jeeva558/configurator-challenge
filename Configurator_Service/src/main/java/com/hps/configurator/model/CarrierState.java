package com.hps.configurator.model;

import java.util.List;

public class CarrierState
{
	private String carrier_name;
	private List<CarrierStateType> carrierStateType;
	
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public List<CarrierStateType> getCarrierStateType() {
		return carrierStateType;
	}
	public void setCarrierStateType(List<CarrierStateType> carrierStateType) {
		this.carrierStateType = carrierStateType;
	}
	
	
}
