package com.hps.configurator.model;

public class EdiStates 
{
	private int carrier_id;
	private String businessTypeValue;
	public int getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(int carrier_id) {
		this.carrier_id = carrier_id;
	}
	public String getBusinessTypeValue() {
		return businessTypeValue;
	}
	public void setBusinessTypeValue(String businessTypeValue) {
		this.businessTypeValue = businessTypeValue;
	}
	
	
}
