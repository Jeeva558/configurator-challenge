package com.hps.configurator.model;

public class EdiData {
private String businessTypeValue;
private String stateValue;
private String tpaId;
private String hiosId;
private String payeeId;
private String senderId;
private String qhpId;
private String taxId;
private String receiverId;
/**
 * @return the businessTypeValue
 */
public String getBusinessTypeValue() {
	return businessTypeValue;
}
/**
 * @param businessTypeValue the businessTypeValue to set
 */
public void setBusinessTypeValue(String businessTypeValue) {
	this.businessTypeValue = businessTypeValue;
}
/**
 * @return the stateValue
 */
public String getStateValue() {
	return stateValue;
}


/**
 * @param stateValue the stateValue to set
 */
public void setStateValue(String stateValue) {
	this.stateValue = stateValue;
}
/**
 * @return the tpaId
 */
public String getTpaId() {
	return tpaId;
}
/**
 * @param tpaId the tpaId to set
 */
public void setTpaId(String tpaId) {
	this.tpaId = tpaId;
}
/**
 * @return the hiosId
 */
public String getHiosId() {
	return hiosId;
}
/**
 * @param hiosId the hiosId to set
 */
public void setHiosId(String hiosId) {
	this.hiosId = hiosId;
}
/**
 * @return the payeeId
 */
public String getPayeeId() {
	return payeeId;
}
/**
 * @param payeeId the payeeId to set
 */
public void setPayeeId(String payeeId) {
	this.payeeId = payeeId;
}
/**
 * @return the senderId
 */
public String getSenderId() {
	return senderId;
}
/**
 * @param senderId the senderId to set
 */
public void setSenderId(String senderId) {
	this.senderId = senderId;
}
/**
 * @return the qhpId
 */
public String getQhpId() {
	return qhpId;
}
/**
 * @param qhpId the qhpId to set
 */
public void setQhpId(String qhpId) {
	this.qhpId = qhpId;
}
/**
 * @return the taxId
 */
public String getTaxId() {
	return taxId;
}
/**
 * @param taxId the taxId to set
 */
public void setTaxId(String taxId) {
	this.taxId = taxId;
}
/**
 * @return the receiverId
 */
public String getReceiverId() {
	return receiverId;
}
/**
 * @param receiverId the receiverId to set
 */
public void setReceiverId(String receiverId) {
	this.receiverId = receiverId;
}

}
