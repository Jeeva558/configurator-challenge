package com.hps.configurator.model;

public class CarrierRemittance 
{
private String Carrier_name;
private String Bill_logo_code;
private String Bill_question_tel;
private String Bill_question_website;
private String Payable_name;
private String remit_name;
private String Address_line1;
private String Address_line2;
private String City;
private String State;
private String Zip;
public String getCarrier_name() {
	return Carrier_name;
}
public void setCarrier_name(String carrier_name) {
	Carrier_name = carrier_name;
}
public String getBill_logo_code() {
	return Bill_logo_code;
}
public void setBill_logo_code(String bill_logo_code) {
	Bill_logo_code = bill_logo_code;
}
public String getBill_question_tel() {
	return Bill_question_tel;
}
public void setBill_question_tel(String bill_question_tel) {
	Bill_question_tel = bill_question_tel;
}
public String getBill_question_website() {
	return Bill_question_website;
}
public void setBill_question_website(String bill_question_website) {
	Bill_question_website = bill_question_website;
}
public String getPayable_name() {
	return Payable_name;
}
public void setPayable_name(String payable_name) {
	Payable_name = payable_name;
}
public String getRemit_name() {
	return remit_name;
}
public void setRemit_name(String remit_name) {
	this.remit_name = remit_name;
}
public String getAddress_line1() {
	return Address_line1;
}
public void setAddress_line1(String address_line1) {
	Address_line1 = address_line1;
}
public String getAddress_line2() {
	return Address_line2;
}
public void setAddress_line2(String address_line2) {
	Address_line2 = address_line2;
}
public String getCity() {
	return City;
}
public void setCity(String city) {
	City = city;
}
public String getState() {
	return State;
}
public void setState(String state) {
	State = state;
}
public String getZip() {
	return Zip;
}
public void setZip(String zip) {
	Zip = zip;
}


}
