package com.hps.configurator.model;

import java.util.List;

public class CarrierBusiness 
{
	private String carriername;
	private List<String> individual;
	private List<String> group;
	public String getCarriername() {
		return carriername;
	}
	public void setCarriername(String carriername) {
		this.carriername = carriername;
	}
	public List<String> getIndividual() {
		return individual;
	}
	public void setIndividual(List<String> individual) {
		this.individual = individual;
	}
	public List<String> getGroup() {
		return group;
	}
	public void setGroup(List<String> group) {
		this.group = group;
	}
	
	
	

}
