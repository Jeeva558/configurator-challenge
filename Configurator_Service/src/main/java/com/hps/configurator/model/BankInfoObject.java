package com.hps.configurator.model;

import java.sql.Date;

public class BankInfoObject {
	private int carrierId;
	private String stateCode;
	private String bankAccountTypeId;
	private String bankName;
	private String bankAccountNo;
	private String bankNumCode;
	private String batchCoId;
	private String batchCoName;
	private String merchantID;
	private String securityKey;
	private Date securityExpirationDate;
	private String siteCode;
	/**
	 * @return the carrierId
	 */
	public int getCarrierId() {
		return carrierId;
	}
	/**
	 * @param carrierId the carrierId to set
	 */
	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}
	/**
	 * @return the stateCode
	 */
	public String getstateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setstateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return the bankAccountTypeId
	 */
	public String getBankAccountTypeId() {
		return bankAccountTypeId;
	}
	/**
	 * @param bankAccountTypeId the bankAccountTypeId to set
	 */
	public void setBankAccountTypeId(String bankAccountTypeId) {
		this.bankAccountTypeId = bankAccountTypeId;
	}
	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * @return the bankAccountNo
	 */
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	/**
	 * @param bankAccountNo the bankAccountNo to set
	 */
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	/**
	 * @return the bankNumCode
	 */
	public String getbankNumCode() {
		return bankNumCode;
	}
	/**
	 * @param bankNumCode the bankNumCode to set
	 */
	public void setbankNumCode(String bankNumCode) {
		this.bankNumCode = bankNumCode;
	}
	/**
	 * @return the batchCoId
	 */
	public String getbatchCoId() {
		return batchCoId;
	}
	/**
	 * @param batchCoId the batchCoId to set
	 */
	public void setbatchCoId(String batchCoId) {
		this.batchCoId = batchCoId;
	}
	/**
	 * @return the batchCoName
	 */
	public String getbatchCoName() {
		return batchCoName;
	}
	/**
	 * @param batchCoName the batchCoName to set
	 */
	public void setbatchCoName(String batchCoName) {
		this.batchCoName = batchCoName;
	}
	/**
	 * @return the merchantID
	 */
	public String getmerchantID() {
		return merchantID;
	}
	/**
	 * @param merchantID the merchantID to set
	 */
	public void setmerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	/**
	 * @return the securityKey
	 */
	public String getSecurityKey() {
		return securityKey;
	}
	/**
	 * @param securityKey the securityKey to set
	 */
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	/**
	 * @return the securityExpirationDate
	 */
	public Date getsecurityExpirationDate() {
		return securityExpirationDate;
	}
	/**
	 * @param securityExpirationDate the securityExpirationDate to set
	 */
	public void setsecurityExpirationDate(Date securityExpirationDate) {
		this.securityExpirationDate = securityExpirationDate;
	}
	/**
	 * @return the siteCode
	 */
	public String getSiteCode() {
		return siteCode;
	}
	/**
	 * @param siteCode the siteCode to set
	 */
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	

}
