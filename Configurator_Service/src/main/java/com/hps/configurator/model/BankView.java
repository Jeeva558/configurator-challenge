package com.hps.configurator.model;

public class BankView 
{
	private String stateCode;
	private String bankAccountType;
	private String bankName;
	private String bankAccountNumber;
	private String bankNumberCode;
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getBankAccountType() {
		return bankAccountType;
	}
	public void setBankAccountType(String bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getBankNumberCode() {
		return bankNumberCode;
	}
	public void setBankNumberCode(String bankNumberCode) {
		this.bankNumberCode = bankNumberCode;
	}
	
	
	
}
