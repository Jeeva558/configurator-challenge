package com.hps.configurator.model;

import java.util.List;

public class EdiObject {
private int carrierId;
private List<EdiData> ediData;
/**
 * @return the carrierId
 */
public int getCarrierId() {
	return carrierId;
}
/**
 * @param carrierId the carrierId to set
 */
public void setCarrierId(int carrierId) {
	this.carrierId = carrierId;
}
/**
 * @return the ediData
 */
public List<EdiData> getEdiData() {
	return ediData;
}
/**
 * @param ediData the ediData to set
 */
public void setEdiData(List<EdiData> ediData) {
	this.ediData = ediData;
}

}
