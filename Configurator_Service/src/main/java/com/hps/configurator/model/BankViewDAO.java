package com.hps.configurator.model;

public class BankViewDAO 
{
	private String state_short_name;
	private String bank_account_type_description;
	private String bank_name;
	private String bank_account_number;
	private String bank_number_code;
	public String getState_short_name() {
		return state_short_name;
	}
	public void setState_short_name(String state_short_name) {
		this.state_short_name = state_short_name;
	}
	public String getBank_account_type_description() {
		return bank_account_type_description;
	}
	public void setBank_account_type_description(String bank_account_type_description) {
		this.bank_account_type_description = bank_account_type_description;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getBank_account_number() {
		return bank_account_number;
	}
	public void setBank_account_number(String bank_account_number) {
		this.bank_account_number = bank_account_number;
	}
	public String getBank_number_code() {
		return bank_number_code;
	}
	public void setBank_number_code(String bank_number_code) {
		this.bank_number_code = bank_number_code;
	}
	
	
}
