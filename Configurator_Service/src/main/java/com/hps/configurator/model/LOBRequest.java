package com.hps.configurator.model;

import java.util.List;

public class LOBRequest 
{
	private String carrier_name;
	private List<String> lob;
	
	
	public String getCarrier_name() {
		return carrier_name;
	}

	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}

	public List<String> getLob() {
		return lob;
	}

	public void setLob(List<String> lob) {
		this.lob = lob;
	}
	
	
	
}
