package com.hps.configurator.model;

import java.util.List;

public class CarrierStateType 
{
	private String business_type;
	private String business_sub_type;
	private List<String> states;
	public String getBusiness_type() {
		return business_type;
	}
	public void setBusiness_type(String business_type) {
		this.business_type = business_type;
	}
	public String getBusiness_sub_type() {
		return business_sub_type;
	}
	public void setBusiness_sub_type(String business_sub_type) {
		this.business_sub_type = business_sub_type;
	}
	public List<String> getStates() {
		return states;
	}
	public void setStates(List<String> states) {
		this.states = states;
	}

	
}
