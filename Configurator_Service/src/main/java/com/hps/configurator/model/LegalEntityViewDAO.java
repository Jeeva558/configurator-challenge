package com.hps.configurator.model;

public class LegalEntityViewDAO 
{
	private String address_1;
	private String address_2;
	private String city;
	private String zip_code;
	private String legal_entity_name;
	private String legal_entity_code;
	private String bank_name;
	private String bank_account_number;
	public String getAddress_1() {
		return address_1;
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getAddress_2() {
		return address_2;
	}
	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip_code() {
		return zip_code;
	}
	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}
	public String getLegal_entity_name() {
		return legal_entity_name;
	}
	public void setLegal_entity_name(String legal_entity_name) {
		this.legal_entity_name = legal_entity_name;
	}
	public String getLegal_entity_code() {
		return legal_entity_code;
	}
	public void setLegal_entity_code(String legal_entity_code) {
		this.legal_entity_code = legal_entity_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getBank_account_number() {
		return bank_account_number;
	}
	public void setBank_account_number(String bank_account_number) {
		this.bank_account_number = bank_account_number;
	}	
	
	
	
}
