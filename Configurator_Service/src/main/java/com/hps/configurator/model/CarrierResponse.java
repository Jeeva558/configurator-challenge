package com.hps.configurator.model;

public class CarrierResponse 
{
	 private String status;
	 private int carrier_id;
	 
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(int carrier_id) {
		this.carrier_id = carrier_id;
	}
	 
	 
}
