package com.hps.configurator.model;

public class CarrierInfo 
{
private String carriername;
private String carriercode;

public String getCarriername() {
	return carriername;
}
public void setCarriername(String carriername) {
	this.carriername = carriername;
}
public String getCarriercode() {
	return carriercode;
}
public void setCarriercode(String carriercode) {
	this.carriercode = carriercode;
}

}
