package com.hps.configurator.configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DataSourceConfigurations {

	@Bean(name="configuratorDataStore")
	@ConfigurationProperties(prefix="configurator-database.datasource")
	public DataSource createConfiguratorDataSource(){
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name="configuratorDataSourceTemplate")
	@Autowired
	public JdbcTemplate createJdbcTemplateForConfigurator(@Qualifier("configuratorDataStore") DataSource configuratorDataSource){
		return new JdbcTemplate(configuratorDataSource);
	}
}
