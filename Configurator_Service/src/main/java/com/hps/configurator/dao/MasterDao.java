package com.hps.configurator.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.hps.configurator.model.AllCarriers;
import com.hps.configurator.model.BankViewDAO;
import com.hps.configurator.model.LegalEntityViewDAO;

public interface MasterDao 
{
	public List<String> getCarrierCodes();
	public List<String> getAllStates();
	public void addcarrier(String CarrierName,String CarrierCode);
	public int getCarrierID(String carriername);
	public int selectbusinessTypeId(String business_type,String business_sub_type);
	public void addCarrierRemit(Object[] object);
	public void addCarrierBusiness(Object[] object);
	public int getOwningCarrierCode(String state);
	public void addCarrierStateWide(Object[] object);
	public List<String> getLineOfBusiness();
	public void addLOB(Object[] object);
	public int getBankId(String bankAccountType,String bankName,String bankAccountNumber);
	public List<String> getStateCodeDao();
	public int getBankAccountTypeIdDao(String bankAccountTypeId);
	public List<String> getBankAccountTypeDescDao();
	public void updateBankDetailsService(int carrierId,String stateShortName,int bankAccountTypeId,String bankName,String bankAccountNo,String bankNumberCode,
			String batchCompanyId,String batchCompanyName,String merchantId,String securityKey,Date securityExpDate,int isCarrierAcccountUsed, String siteCode, String updatedBy) ;
	public List<Map<String, Object>> getBusinessTypeDao(int carrierId);
	public List<String> getEdiStatesDao(int carrierId,String businessTypeCol,String businessTypeSubTypeCol);
	public List<String> getOwningCarrierDao(int carrierId);
	public int getBusinessTypeIdDao(String businessTypeCol, String businessTypeSubTypeCol);
	public int getOwningCarrierIdDao(int carrierId, int businessTypeId, String owningCarrier, String stateShortName) ;
	public void updateCarrierStatewide(Object[] object);
	public void updateCarrierContact(int carrierId, int carrierLegalEntityId, String stateShortName,
			int addressTypeId, String address1, String address2, String city, String zipCode, String tel,
			String telExtension, String fax, String email, String workingHours,String updatedBy) ;
	public List<Map<String, Object>> getBankInfoDao(int carrierId);
	public List<String> getAddressTypeDao();
	public int updateCarrierLegalEntity(int carrierId,int bankID,String legalEntityName,String legalEntityCode, String stateShortName, String webUrl,String updatedBy);
	public int getCarrierId(String carrierName,String carrierCode);
	public int getAddressTypeId(String addressType);
	public List<String> getAllProductType();
	public List<String> getAllLegalEntityStates(int carrierId);
	public List<String> getallProducts(int carrierId);
	public List<String> getLegalEntityNames(int carrierId);
	public int getProductCodeID(String prodTypeCode);
    public int getLineOfBusinessID(String prodType);
    public int getLegalEntityID(String legalEntity);
    public void addLegalEntity2(Object[] object);
	public List<AllCarriers> getAllCarrierCodeDao();
	public List<BankViewDAO> getBankDetails(int carrierId);
	public List<LegalEntityViewDAO> getLegalEntityDetails(int carrierId);
	public List<String> getIndividualLOB(int carrierId);

}
