package com.hps.configurator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import com.hps.configurator.utilities.ConfigUtility;



@SpringBootApplication
public class ConfiguratorServiceApplication {

	public static void main(String[] args) {
//		SpringApplication.run(ConfiguratorServiceApplication.class, args);
		
		Properties defaultProperties =  new Properties();
		try {
			defaultProperties = ConfigUtility.getConfigurations();
			ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(ConfiguratorServiceApplication.class).properties(defaultProperties).build().run(args);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Application has not been started as Config Files weren't found");

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Application has not been started due to IO Exception on Config Read");

		}finally{
			System.out.println("Finally I would like to thank you for taking the last 5 seconds to read this. :) Velle BC!");
		}
			
	}

}
