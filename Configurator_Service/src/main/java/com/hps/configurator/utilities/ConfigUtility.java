package com.hps.configurator.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigUtility 
{
	public static Properties getConfigurations() throws FileNotFoundException, IOException{
		Properties properties = new Properties();
		properties.load(new FileInputStream("C:/mnt/configs/config.properties"));
		properties.load(new FileInputStream("C:/mnt/configs/dataSource.properties"));
		properties.load(new FileInputStream("C:/mnt/configs/sqlQuery.properties"));
		return properties;
	}
}
