package com.hps.configurator.utilities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.hps.configurator.model.BankView;
import com.hps.configurator.model.BankViewDAO;
import com.hps.configurator.model.LegalEntityView;
import com.hps.configurator.model.LegalEntityViewDAO;

@Component
public class Utility 
{

	public List<BankView> alignBankdetails(List<BankViewDAO> bankdao)
	{
		List<BankView> bankview=new ArrayList<BankView>();
		for(BankViewDAO singlebank:bankdao)
		{
			BankView bv=new BankView();
			bv.setStateCode(checkNull(singlebank.getState_short_name()));
			bv.setBankAccountType(checkNull(singlebank.getBank_account_type_description()));
			bv.setBankName(checkNull(singlebank.getBank_name()));
			bv.setBankAccountNumber(checkNull(singlebank.getBank_account_number()));
			bv.setBankNumberCode(checkNull(singlebank.getBank_number_code()));
			bankview.add(bv);
		}
		return bankview;
	}
	
	public String checkNull(String value)
	{
		if(!value.isEmpty()&&value!=null)
		{
			return value;
		}
		else
		{
			return "";
		}
	}
	
	public String commacheck(String value)
	{
		if(!value.isEmpty()&&value!=null)
		{
			return value+",";
		}
		else
		{
			return "";
		}
	}
	
	public List<LegalEntityView> alignLegalEntityDetail(List<LegalEntityViewDAO> leDao)
	{
		List<LegalEntityView> leView=new ArrayList<LegalEntityView>();
		for(LegalEntityViewDAO Singlele:leDao)
		{
			LegalEntityView lev=new LegalEntityView();
			lev.setLegalEntityName(checkNull(Singlele.getLegal_entity_name()));
			lev.setLegalEntityCode(checkNull(Singlele.getLegal_entity_code()));
			String bankd=commacheck(Singlele.getBank_name())+checkNull(Singlele.getBank_account_number());
			lev.setBankDetails(bankd);
			String address=commacheck(Singlele.getAddress_1())+commacheck(Singlele.getAddress_2())+commacheck(Singlele.getCity())+checkNull(Singlele.getZip_code());
			lev.setAddress(address);
			leView.add(lev);
		}
		return leView;
	}
}
