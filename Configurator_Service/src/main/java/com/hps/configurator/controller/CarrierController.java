package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.AllCarriers;
import com.hps.configurator.model.CarrierInfo;
import com.hps.configurator.model.CarrierResponse;
import com.hps.configurator.service.MasterService;


@CrossOrigin
@RestController
@RequestMapping("/Carrier")
public class CarrierController 
{
	@Autowired
	MasterService masterService;
	
	@GetMapping("/getCarrierCode")
	public List<String> getAvailableCarrierCode()
	{
		List<String> carriercode=masterService.getCarrierCode();
		return carriercode;
	}
	@GetMapping("/getCarriers")
	public List<AllCarriers> getAllCarrierCode()
	{
		List<AllCarriers> carriercode=masterService.getAllCarrierCode();
		return carriercode;
	}
	@PostMapping("/addCarrier")
	public int addcarrier(@RequestBody CarrierInfo carrierinfo)
	{	CarrierResponse cr=new CarrierResponse();
		try
		{
		int carrier_id=masterService.addcarrier(carrierinfo);
		cr.setCarrier_id(carrier_id);
		cr.setStatus("Success");
		return carrier_id;	
		}
		catch(Exception e)
		{
			cr.setCarrier_id(-1);
			cr.setStatus("Error");
			return -1;
		}
		
		
		
	}
//	@GetMapping("/getCarrierId/{carrierCode}/{carrierName}")
//	public String getCarrierId(@PathVariable String carrierCode,@PathVariable String carrierName)
//	{
//		String carrierId=masterService.getCarrierId(carrierCode,carrierName);
//		return carrierId;
//	}
}
