package com.hps.configurator.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.CarrierBusiness;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/CarrierBusinessAPI")
public class CarrierBusinessContoller 
{
	@Autowired
	MasterService masterService;
	
	@PostMapping("/addCarrierBusiness")
	public String addCarrierBusiness(@RequestBody CarrierBusiness carrierBusiness)
	{
		System.out.println("AddCarrierBusiness Process Started");
		try
		{
			masterService.addCarrierBusiness(carrierBusiness);
			System.out.println("AddCarrierBusiness Process Ended");
			return "{\"status\":\"success\"}";
		}
		catch(Exception e)
		{
			return "{\"status\":\"error\"}";
		}
	}
}
