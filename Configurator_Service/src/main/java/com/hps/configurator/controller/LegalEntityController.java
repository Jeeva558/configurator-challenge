package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.exception.BaseException;
import com.hps.configurator.model.LegalEntityObject1;
import com.hps.configurator.model.LegalEntityObject2;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/configurator")
public class LegalEntityController {
	@Autowired
	MasterService masterService;
	
	@RequestMapping("/getBankInfo/{carrierId}")
	public List<String> getBankAccountTypeDesc(@PathVariable int carrierId) throws BaseException
	{
		List<String> bankAccountTypeDesc=null;
			bankAccountTypeDesc= masterService.getBankInfoService(carrierId);
				return bankAccountTypeDesc;
	}
	@GetMapping("/getAddressType")
	public List<String> getAddressType()
	{
		return masterService.getAddressType();
	}
	@PostMapping("/postLegalEntity")
	public String postBankDetails(@RequestBody LegalEntityObject1 legalObjectList)
	{
		System.out.println("Inside "+ legalObjectList.getLegalEntityName());
		masterService.postLegalEntityService(legalObjectList);
		return "{\"Status\":\"Success\"}";
	}
	
	@GetMapping("/getAllLegalEntityStates/{carrierId}")
	public List<String> getAllLegalEntityStates(@PathVariable int carrierId )
	{
		return masterService.getAllLegalEntityStates(carrierId);
	}
	
	@GetMapping("/getAllProductType")
	public List<String> getAllProductType()
	{
		return masterService.getAllProductType();
	}
	
	@GetMapping("/getAllProducts/{carrierId}")
	public List<String> getAllProducts(@PathVariable int carrierId)
	{
		return masterService.getallProducts(carrierId);
	}
	
	@GetMapping("/getLegalEntityNames/{carrierId}")
	public List<String> getLegalEntityNames(@PathVariable int carrierId)
	{
		return masterService.getLegalEntityNames(carrierId);
	}
	@PostMapping("/addLegalEntity2")
    public String addLegalEntity2(@RequestBody LegalEntityObject2 legalEntity2)
    {
           try
           {
                  masterService.addLegalEntity2(legalEntity2);
                  return "{\"Status\":\"Success\"}";
           }
           catch(Exception e)
           {
                  return "{\"Status\":\"error\"}";
           }
    }

}
