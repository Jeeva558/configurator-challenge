package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.EdiObject;
import com.hps.configurator.model.EdiStates;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/configurator")
public class EdiController {
	@Autowired
	MasterService masterService;
	
	@RequestMapping("/getBusinessType/{carrierId}")
	public List<String> getBusinessType(@PathVariable int carrierId)
	{
		return masterService.getBusinessTypeService(carrierId);
	}
	@RequestMapping("/getOwningCarriers/{carrierId}")
	public List<String> getOwningCarriers(@PathVariable int carrierId)
	{
		return masterService.getOwningCarriersService(carrierId);
	}
	@PostMapping("/getEdiStates")
	public List<String> getEdiStates(@RequestBody EdiStates edistates)
	{
		return masterService.getEdiStatesService(edistates);
	}
	@PostMapping("/postEdi")
	public String postEdiDetails(@RequestBody EdiObject ediObject)
	{
		System.out.println("Inside "+ ediObject.getCarrierId());
		masterService.postEdiDetailsService(ediObject);
		return "{\"Status\":\"Success\"}";
	}
}
