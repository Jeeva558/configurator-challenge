package com.hps.configurator.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hps.configurator.model.AvailableEnvironment;
import com.hps.configurator.model.CarrierConfigValue;


@CrossOrigin
@RestController
@RequestMapping("/ConfirmationPage")
public class ConfirmationPageController {
	@Value("${availableEnvironment}")
	String availableEnvironment;
	@Value("${carrierConfigValue}")
	String carrierConfigValue;
	@Value("${getProcessorManager}")
	String getProcessorManager;
	@GetMapping("/getAvailableEnvironment")
	public String getAvailableCarrierCode()
	{
		RestTemplate restTemplate = new RestTemplate();
	ResponseEntity<String> result = restTemplate.getForEntity(availableEnvironment, String.class);
	System.out.println(result.getBody());
		
	return result.getBody();
	}
		@GetMapping("/loadCarrierConfigValue/{carrierId}")
		public String loadCarrierConfigValue(@PathVariable String carrierId)
		{
			System.out.println("loadCarrierConfigValue = "+carrierId);
			RestTemplate restTemplate = new RestTemplate();
			System.out.println(carrierConfigValue+carrierId);
			ResponseEntity<String> result = restTemplate.getForEntity(carrierConfigValue+carrierId,String.class);
			System.out.println(result.getBody());
			return result.getBody();
			//return "";
		}
		@GetMapping("/getProcessorManager/{carrierId}/{regionId}")
		public String getProcessorManager(@PathVariable String carrierId,@PathVariable String regionId)
		{
			System.out.println("getProcessorManager = "+carrierId);
			RestTemplate restTemplate = new RestTemplate();
			String url=getProcessorManager+carrierId+"/"+regionId;
			System.out.println(url);
			ResponseEntity<String> result = restTemplate.getForEntity(url,String.class);
			System.out.println(result.getBody());
			return result.getBody();
			//return "";
		}
}
