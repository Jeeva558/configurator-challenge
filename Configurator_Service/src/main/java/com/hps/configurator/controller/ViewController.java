package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.BankView;
import com.hps.configurator.model.BusinessView;
import com.hps.configurator.model.LegalEntityView;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/View")
public class ViewController 
{
	@Autowired
	MasterService masterService;

	@GetMapping("/getAllStates/{carrierId}")
	public List<BusinessView> getallStates(@PathVariable int carrierId)
	{
		System.out.println("Inside getallStates = "+carrierId);
		return masterService.getStateDetails(carrierId);
	}
	
	@GetMapping("/getBankDetails/{carrierId}")
	public List<BankView> getBankDetails(@PathVariable int carrierId)
	{
		
		return masterService.getBankDetails(carrierId);
		
	}
	
	@GetMapping("/getLegalEntity/{carrierId}")
	public List<LegalEntityView> getLegalEntityDetails(@PathVariable int carrierId)
	{
		return masterService.getlegalEntityDetails(carrierId);
	}
	
	
}
