package com.hps.configurator.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.CarrierState;
import com.hps.configurator.model.CarrierStateType;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/StateAPI")
public class StateController 
{
	@Autowired
	MasterService masterservice;

	@GetMapping("/allStates")
	public List<String> getallStates()
	{
		return masterservice.getAllstates();
	}
	
	@PostMapping("/addStates")
	public String addallStates(@RequestBody CarrierState carrierstate)
	{
		System.out.println(carrierstate.getCarrier_name());
		masterservice.addStates(carrierstate);
		return carrierstate.getCarrier_name();
		
	}
}
