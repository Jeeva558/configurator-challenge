package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.BankInfoObject;
import com.hps.configurator.service.MasterService;


@CrossOrigin
@RestController
@RequestMapping("/configurator")
public class BankInfoController {
	@Autowired
	MasterService masterService;
	
	@RequestMapping("/getStateCode")
	public List<String> getStateCode()
	{
		return masterService.getStateCodeService();
	}
	
	@RequestMapping("/getBankAccountTypeDesc")
	public List<String> getBankAccountTypeDesc()
	{
		return masterService.getBankAccountTypeDescService();
	}
	@PostMapping("/postBankDetails")
	public String postBankDetails(@RequestBody List<BankInfoObject> bankInfoObjectList)
	{
		System.out.println("Inside "+ bankInfoObjectList.get(0).getstateCode());
		masterService.postBankDetailsService(bankInfoObjectList);
		return "{\"Status\":\"Success\"}";
	}
}
