package com.hps.configurator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.LOBRequest;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/LOBAPI")
public class LineOfBusinessController 
{
	@Autowired
	MasterService masterService;
	
	@GetMapping("/getLineOfBusiness")
	public List<String> getLOB()
	{
		return masterService.getLOB();
	}
	
	@RequestMapping("/getIndividualLineOfBusiness/{carrierId}")
	public String getIndividualLOB(@PathVariable int carrierId)
	{
		return masterService.getIndividualLOB(carrierId);
	}
	
	@PostMapping("/addLineOfBusiness")
	public String addSelectedLOB(@RequestBody LOBRequest lobrequest)
	{
		try
		{
			masterService.addLineOfBusiness(lobrequest);
			return "{\"status\":\"success\"}";
		}
		catch(Exception e)
		{
			return "{\"status\":\"error\"}";
		}
	}
}
