package com.hps.configurator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hps.configurator.model.CarrierRemittance;
import com.hps.configurator.service.MasterService;

@CrossOrigin
@RestController
@RequestMapping("/CarrierRemit")
public class CarrierRemitController 
{
	@Autowired
	MasterService masterservice;
	
	@PostMapping("/addRemittanceDetails")
	public String addRemittanceDetails(@RequestBody CarrierRemittance carrierremit)
	{
		System.out.println("CarrierName for addRemittanceDetails"+carrierremit.getCarrier_name());
		masterservice.addRemittanceDetails(carrierremit);
		return "{\"status\":\"success\"}";
	}
	
	@GetMapping("/getCarrierID/{carriername}")
	public Integer getCarrierID(@PathVariable("carriername") String carriername)
	{
		return masterservice.getCarrierID(carriername);
	}
	
}
